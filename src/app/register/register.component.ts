import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { AlertService } from '../services/alert.service';
import {FormBuilder, FormGroup, FormControl, Validators, AbstractControl} from "@angular/forms";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers : [UserService, AlertService]
})

export class RegisterComponent implements OnInit {
  form: FormGroup;
 	model: any = {};
  loading = false;
  constructor(
    private router: Router,
    private userService: UserService,
    private alertService: AlertService
  ) { }

  
 	ngOnInit() {

    this.form = new FormGroup({

      email : new FormControl('', Validators.required);
      first_name : new FormControl('', Validators.required);
      last_name : new FormControl('', Validators.required);
      username : new FormControl('', Validators.required);
      password : new FormControl('', Validators.required);
      password_confirmation : new FormControl('', [Validators.required, this.equalto('password')]);
      job_title : new FormControl('', Validators.required);
      avatar : new FormControl('');

      });
    }

    register(form) {

        this.loading = true;
        form.tos = true;
        
        this.userService.create(form)
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
                
    }

    onFileChange(event) {

      let reader = new FileReader();

      if(event.target.files && event.target.files.length > 0) {
        let file = event.target.files[0];
        reader.readAsDataURL(file);

        reader.onload = () => {
          this.form.get('avatar').setValue({
            filename: file.name,
            filetype: file.type,
            value: reader.result.split(',')[1]
          })
           
        };

      }
  }

  clearFile() {
    this.form.get('avatar').setValue(null);
    this.fileInput.nativeElement.value = '';
  }

  equalto(field_name): ValidatorFn {

    return (control: AbstractControl): {[key: string]: any} => {
    let input = control.value;
    let isValid=control.root.value[field_name]==input
    if(!isValid)
    return { 'equalTo': {isValid} }
      else
      return null;
    };
  }
}
