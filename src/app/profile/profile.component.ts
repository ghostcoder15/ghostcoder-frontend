import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [UserService]
})
export class ProfileComponent implements OnInit {

  	currentUser: User;
    user: User[] = [];

    constructor(private userService: UserService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
    	setTimeout( => (){
    		 //this.loadprofile(); 
    	}, 3000);
        
    }

    private loadprofile() {

		this.userService.getprofile(this.currentUser.token).subscribe(user => { this.user = user; });
    	
   	 }
}
