export class User {
    id: number;
    email:string;
    username: string;
    password: string;
    password_confirmation:string;
    first_name: string;
    last_name: string;
    job_title : string;
    tos:string;
}