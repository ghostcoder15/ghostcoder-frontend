import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User } from '../models/user';

@Injectable()
export class UserService {

    constructor(private http: HttpClient) 
    { }

    create(user : User) {
        return this.http.post('http://localhost/laravel-login-and-user-management/public/api/register', user);
    }

    login(user : User){
    	return this.http.post('http://localhost/laravel-login-and-user-management/public/api/login', user);
    }

    getprofile(token:string){
   	
   		let headers = new HttpHeaders({'Content-Type': 'application/json','Accept': 'application/json','Authorization':'Bearer '+token});
    	return this.http.get('http://localhost/laravel-login-and-user-management/public/api/me', {headers:headers});
    }

    
}