import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from "@angular/router";

import { FormsModule, FormProviders, ReactiveFormsModule} from '@angular/forms';
//import { FileUploadModule } from "ng2-file-upload";

import { AppComponent } from './app.component';
import { AlertComponent } from './directives/alert.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

import { UserService } from './services/user.service';
import { HttpClientModule } from '@angular/common/http';
import { AlertService } from './services/alert.service';
import { ProfileComponent } from './profile/profile.component';


const routes: Routes = [
 { path: 'register', component: RegisterComponent },
 { path: 'login', component: LoginComponent },
 { path: 'profile', component: ProfileComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule],
  providers: [
    UserService,
    AlertService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
